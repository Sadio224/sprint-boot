package ensa.etudiant.springProject.controller;

import java.util.LinkedList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ensa.etudiant.springProject.Etudiant;


@RestController
public class EtudiantController {

	LinkedList<Etudiant> eList = new LinkedList<Etudiant>();
	
	@CrossOrigin(origins = "http://localhost:3000", allowedHeaders = "http://localhost:3000")
	@GetMapping(value="/1")
	public LinkedList<Etudiant> EtudiantJson() {
		
		
		eList.add(new Etudiant("Sow", 111111, "Genie Informatique"));
		eList.add(new Etudiant("baldé", 222222, "Genie Informatique"));
		eList.add(new Etudiant("nanamou", 333333, "Genie Informatique"));
		eList.add(new Etudiant("diallo", 777777, "Genie Informatique"));
		return eList;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value="/2",method = RequestMethod.POST)
	public Etudiant EtudiantSave(@RequestBody Etudiant e) {
		eList.add(e);
		System.out.println(e.toString() + " saved");
		return e;
	}
}
