package ensa.etudiant.springProject;

public class Etudiant {

	protected String name;
	protected int cin;
	protected String filiere;
	protected String all;
	
	public Etudiant(String name, int cin, String filiere) {
		super();
		this.name = name;
		this.cin = cin;
		this.filiere = filiere;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCin() {
		return cin;
	}
	public void setCin(int cin) {
		this.cin = cin;
	}
	public String getFiliere() {
		return filiere;
	}
	public void setFiliere(String filiere) {
		this.filiere = filiere;
	}
	
	public String toString() {
		return name + ", " + cin + ", " + filiere;
	}
	
	public String getAll() {
		return toString();
	}
	public void setAll(String all) {
		this.all = all;
	}
	
}
